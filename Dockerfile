FROM python:2.7
MAINTAINER blurrcat@gmail.com
EXPOSE 8000

ENV CHEETAH_DEBUG=False
ENV WORKERS=2

RUN mkdir /usr/app
WORKDIR /usr/app
ADD requirements.txt ./
RUN pip install -r requirements.txt
ADD . .
RUN pip install -e . && \
    gcc -fPIC -c libcaptcha.c && \
    gcc -shared -o cheetah/libcaptcha.so libcaptcha.o

CMD gunicorn -b "0.0.0.0:8000" --access-logfile "-" -w $WORKERS cheetah.app:app
