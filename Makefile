.PHONY: clean-pyc clean-build docs

help:
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "config - install config and scripts in virtualenv"
	@echo "coverage - run coverage test"
	@echo "test - run tests"
	@echo "docs - generate Sphinx HTML documentation, including API docs"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info
	rm -f libcaptcha.o
	rm -f cheetah/libcaptcha.so
	rm -f cheetah/static/*.gif

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +

test:
	coverage run --source cheetah -m py.test -l
	@coverage report

coverage: test
	@coverage html

install:
	gcc -fPIC -c libcaptcha.c
	gcc -shared -o cheetah/libcaptcha.so libcaptcha.o
	pip install -r requirements.txt
	pip install -e .[develop]
