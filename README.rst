Cheetah
=======

.. image:: cheetah.jpg

The cheetah is a large feline inhabiting most of Africa and parts of Iran. It
can run faster than any other land animal - as fast as 112 to 120 km/h.

Cheetah the web service generates and verifies captcha. Aside from the fact
that it runs as fast as the animal, the two are totally irrelevant, no round
black spots involved.

Usage
-----
Get captcha image url. The response is a dict consists of the url of the
image. Example::

    # request
    POST /api/ HTTP/1.1

    # response
    Content-Type: application/json
    Body: {"url": "/static/c50d4ea204ce40ddb896f1fc24831acd.gif"}

Embed this URL in your page. After the client answers, verify here::

    # request, where "USER-INPUT" is what user types in.
    GET /api/verify/c50d4ea204ce40ddb896f1fc24831acd.gif/{USER-INPUT} HTTP/1.1

    # response
    Content-Type: application/json
    Body: {"ok": true, "msg": ""}

The response may be one of the following, depending on if the client has passed
the challenge::

    * {"ok": true, "msg": ""}
    * {"ok": false, "msg": "mismatch"}
    * {"ok": false, "msg": "expired"}

Each captcha image is only effective for 30 seconds.

Credit
------
Harry L. <blurrcat@gmail.com>
