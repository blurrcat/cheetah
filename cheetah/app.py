#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ast
import os
from flask import Flask, jsonify, url_for, request, redirect, render_template
from flask.ext.cache import Cache
from libcapcha import Captcha, CaptchaExpired


app = Flask('cheetah', static_url_path='')
app.config.from_object('cheetah.config')
# production config via env vars
prefix = 'CHEETAH_'
for k, v in os.environ.items():
    if k.startswith(prefix):
        k = k[len(prefix):]
        try:
            v = ast.literal_eval(v)
        except (ValueError, SyntaxError):
            pass
        app.config[k] = v
        m = 'override config %s via environment' % k
        if app.config.get('DEBUG', False):
            m = '%s: %s' % (m, v)
        print m


cache = Cache(app)
captcha = Captcha(app, cache)


@app.route('/')
def index():
    return app.send_static_file('index.html')


@app.route('/api/', methods=['POST'])
def gen():
    return jsonify(url=url_for('static', filename=captcha.generate()))


@app.route('/api/verify/<string:name>/<string:letters>')
def verify(name, letters):
    try:
        ok = captcha.verify(name, letters)
    except CaptchaExpired:
        return jsonify(ok=False, msg='expired')
    else:
        return jsonify(ok=ok, msg='' if ok else 'mismatch')


@app.route('/api/clear')
def clear():
    removed = captcha.clear()
    return jsonify(removed=removed)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
