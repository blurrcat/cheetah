#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

root = os.path.dirname(__file__)
DEBUG = False
SECRET_KEY = '5330809956ef435aaa2b90d02821c7f8'


CAPTCHA_PATH = os.path.join(root, 'libcaptcha.so')
CAPTCHA_EXPIRE = 30  # seconds
CAPTCHA_DIR = os.path.join(root, 'static')

# cache
CACHE_TYPE = 'redis'

