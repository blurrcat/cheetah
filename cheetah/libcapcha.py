#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Python wrapper around libcapcha.
See `http://brokestream.com/captcha.html`_
"""
import ctypes
import glob
import os
import uuid


class CaptchaExpired(BaseException):
    pass


class Captcha(object):
    # those magic numbers are hard coded in the c lib;
    # just don't change them
    IM_SIZE = 200 * 70
    LETTERS = 6
    GIF_SIZE = 17646

    def __init__(self, app=None, cache=None):
        self.app = None
        self.lib = None
        self.dir = None
        self.cache = None
        self.expire = None
        if app and cache:
            self.init_app(app, cache)

    def init_app(self, app, cache):
        self.app = app
        self.cache = cache
        self.expire = app.config['CAPTCHA_EXPIRE']
        self.dir = app.config['CAPTCHA_DIR']
        self.lib = ctypes.CDLL(app.config['CAPTCHA_PATH'])

        self.lib.captcha.argtypes = [
            ctypes.ARRAY(ctypes.c_ubyte, self.IM_SIZE),
            ctypes.ARRAY(ctypes.c_ubyte, self.LETTERS)
        ]
        self.lib.makegif.argtypes = [
            ctypes.ARRAY(ctypes.c_ubyte, self.IM_SIZE),
            ctypes.ARRAY(ctypes.c_ubyte, self.GIF_SIZE)
        ]

    def generate(self):
        """
        generate a captcha in GIF format
        :return:
            a key-value pair, where the key is sent to client-side and the
            value is the encoded string
        """
        im = (ctypes.c_ubyte * self.IM_SIZE)()
        letters = (ctypes.c_ubyte * self.LETTERS)()
        self.lib.captcha(im, letters)
        gif = (ctypes.c_ubyte * self.GIF_SIZE)()
        self.lib.makegif(im, gif)
        # write gif to file
        name = u'{}.gif'.format(uuid.uuid4().hex)
        with open(os.path.join(self.dir, name), 'w') as f:
            f.write(ctypes.string_at(gif, self.GIF_SIZE))
        letters = u''.join(unichr(l) for l in letters[:-1])
        self.cache.set(name, letters, self.expire)
        return name

    def verify(self, name, letters):
        orig = self.cache.get(name)
        if orig:
            return letters == orig
        else:
            raise CaptchaExpired()

    def clear(self):
        i = 0
        for f in glob.glob('{}/*.gif'.format(self.dir)):
            name = os.path.basename(f)
            if not self.cache.get(name):
                try:
                    os.unlink(f)
                except OSError:
                    pass
                else:
                    i += 1
        return i

