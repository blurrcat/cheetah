#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


__version__ = '0.1.0'
readme = open('README.rst').read()


setup(
    name='cheetah',
    version=__version__,
    description='',
    long_description=readme,
    author='Harry L.',
    author_email='blurrcat@gmail.com',
    packages=find_packages(),
    package_dir={'cheetah': 'cheetah'},
)
