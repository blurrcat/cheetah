#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest


@pytest.fixture
def application(request):
    from cheetah.app import app
    ctx = app.app_context()
    ctx.push()
    request.addfinalizer(ctx.pop)
    return app
