#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json


def test_generate(application):
    with application.test_client() as c:
        r = c.post('/api/')
        assert r.status_code == 200
        r = json.loads(r.data)
        assert 'url' in r


def test_verify(application):
    with application.test_client() as c:
        r = c.post('/api/')
        assert r.status_code == 200
        url = json.loads(r.data)['url']
        gid = url.split('/')[-1]
        r = c.get('/api/verify/%s/12345' % gid)
        assert r.status_code == 200
        r = json.loads(r.data)
        assert not r['ok']  # apparently we don't know the right answer
