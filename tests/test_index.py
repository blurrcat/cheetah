#!/usr/bin/env python
# -*- coding: utf-8 -*-


def test_home(application):
    with application.test_client() as c:
        r = c.get('/')
        assert r.status_code == 200
